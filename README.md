## Description

This repository will be used by Jenkins seed job to create scheduled pipeline.

Pipeline will load tags from third party services and send them to Wisetime. Database is used for caching tags and keywords 
(see https://bitbucket.org/WISE-TIME/wt-agent-jenkins-image/src/master/ for details).

Required job param is AGENT_TYPE (e.g. jira, invoice-demo etc). It will be set by seed job. If you need to import with job manually be sure to set it.

This job expects agent to be preinstalled in JENKINS_HOME directory.

Job will be interrupted if not finished in 1437 minutes (1 day minus 3 minutes).
